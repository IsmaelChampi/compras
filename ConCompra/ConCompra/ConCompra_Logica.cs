﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConCompra {
  public class ConCompra_Logica {
     public Logica.Logica cloLogica = new Logica.Logica();
     public AccesoADatos.AccesoADatos cadCAD = new AccesoADatos.AccesoADatos();
     public ConCompra_AccesoADatos proyeDats = new ConCompra_AccesoADatos();


     public bool Main() {

        bool blnReturn = false;

        Comun.Sistema.strLocalAssemblyName = "CONCOMPRA";

        Comun.Sistema.sysPrueba = true;
        if (!cloLogica.GetParameters()) {
           if (ExisteError()) { }
           blnReturn = false;
           return blnReturn;
        }
        // ajusters a tu estructura
        //Comun.Sistema.sysServer = "SIATEC50";

        Comun.Sistema.sysOpCode = ConCompra_Comun.cnsConCompraPpal;
     

        Comun.Sistema.bytCodigoEmpresaReal = Convert.ToByte(Comun.Sistema.sysCodigoEmpresa);

        Comun.Sistema.strPathSistema = @"C:\Siatec\SiatecInfo\";
        Comun.Sistema.strPathEmpresa = @"C:\Siatec\SiatecInfo\G" + Comun.Sistema.sysCodigoGrupo + @"\E" + Comun.Sistema.sysCodigoEmpresa + @"\";
        Comun.Sistema.strPathGrupo = @"C:\Siatec\SiatecInfo\G" + Comun.Sistema.sysCodigoGrupo + @"\E0";



        blnReturn = true;
        return blnReturn;
     }

     public bool ExisteError() {
        bool blnReturn = false;
        if (Comun.Sistema.sysError != 0 || Comun.Sistema.sysMensaje != "") { blnReturn = true; }
        return blnReturn;
     }

     public List<Comun.ProveedorCN> BuscaProvCont(Comun.ProveedorCN ccCont) { return proyeDats.BuscaProv(ccCont); }

     public System.Data.DataTable SelectDTProveedor(reporte ccReport) {
        return proyeDats.SelectDTProveedor(ccReport);
     }

     public List<Empresa> ConsultaDatos() {
        return proyeDats.datos();

     }

     public System.Data.DataTable SelectDTTickett(string numeroFact) {
        return proyeDats.SelectDTTickett(numeroFact);
     }
     public System.Data.DataTable SelectDTPticketDet(string numeroFact) {
        return proyeDats.SelectDTPticketDet(numeroFact);
     }

   }
}
