﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConCompra {
  public class ConCompra_Comun {
     public const string cnsConCompraPpal = "00";
     public static string strCodigoCte { set; get; }
     public static string strNombreCte { set; get; }
   }

  public class sistema {
     public static long SysError { set; get; }
     public static string SysErrorDesc { set; get; }
     public byte[] flujoBytes { set; get; }
     public static bool blGeneraSelect2 { set; get; }
     public static byte bytNoReng { set; get; }
     public static int intNoReng { set; get; }
     public static string sysUser { set; get; }
     public static int AñoServidor { set; get; }
     public static int NumAñosAntes { set; get; }
     public static byte btVisualiza { set; get; }
     public static bool CteAyuda { set; get; }
     public static bool blValidaFoto { set; get; }
  }

  public class ClienteConsulta {
     public string UltimoValorUsado { set; get; }
     public string NombreLLave { set; get; }
     public string SignoComparacion { set; get; }
     public byte NoRengAMostrar { set; get; }
     public string CodigoCte { set; get; }
     public string NombreCte { set; get; }
     public ClienteConsulta() { }
  }

  public class reporte {
     public string Estado { set; get; }
     public string SubTipo { set; get; }
     public bool TodosProveedor { set; get; }
     public string CodigoProveedor { set; get; }
     public string Nombre { set; get; }
     public DateTime FechaDel { set; get; }
     public DateTime FechaAl { set; get; }
     public bool MuestraTodosFactura { set; get; }
     public bool OrdenVendedor { set; get; }
     public string CodigoFact { set; get; }
     public reporte() { }
  }

  public class Empresa {
     public string RFCEM { set; get; }
     public string DireccionEM { set; get; }


     public Empresa() { }
  }

}
