﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConCompra {
   public partial class ReporteCnCpra : Form {
      public reporte locReport = new reporte();
      Logica.Logica cLogica = new Logica.Logica();
      public ConCompra_Logica cloReporte = new ConCompra_Logica();

      public Decimal SubTotal = 0;
      public Decimal IVA16 = 0;
      public Decimal Desc = 0;
      public Decimal SubTotalDesc = 0;
      public Decimal IVA = 0;
      public Decimal Total = 0;
      public Decimal RetIVA = 0;
      public Decimal Isr = 0;
      public Boolean ticket = false;
      public Boolean Agrupacion = false;
      public Boolean AgrupacionParam = false;

      public ReporteCnCpra() {
         InitializeComponent();
      }

      private void ReporteCnCpra_Load(object sender, EventArgs e) {
         DataTable dtProvInfo;
         rv1.ProcessingMode = ProcessingMode.Local;
         rv1.LocalReport.ReportEmbeddedResource = "ConCompra.ReportPrincipal.rdlc";

         dtProvInfo = cloReporte.SelectDTProveedor(locReport);
         this.Text = "Venta Proveedor";

         Microsoft.Reporting.WinForms.ReportDataSource rds = new Microsoft.Reporting.WinForms.ReportDataSource();
         rds.Name = "DataSet1";
         rds.Value = dtProvInfo;

         Microsoft.Reporting.WinForms.ReportParameter[] Param = new Microsoft.Reporting.WinForms.ReportParameter[8];
         Param[0] = new Microsoft.Reporting.WinForms.ReportParameter("prmNombreEmpresa", Comun.Sistema.sysNombreEmpresa);

         Param[1] = new Microsoft.Reporting.WinForms.ReportParameter("prmTituloReporte", "Venta Proveedor" + " Del:  " + locReport.FechaDel.ToString("dd/MM/yyyy").Trim() + " Al: " + locReport.FechaAl.ToString("dd/MM/yyyy").Trim());
         Param[2] = new Microsoft.Reporting.WinForms.ReportParameter("prmFecha", cLogica.GetServerDateTime().ToString("dd/MM/yyyy").Trim());
         Param[3] = new Microsoft.Reporting.WinForms.ReportParameter("prmHora", cLogica.GetServerDateTime().ToString("HH:mm").Trim());
         Param[4] = new Microsoft.Reporting.WinForms.ReportParameter("prmSolicito", Comun.Sistema.sysUser);
         Param[5] = new Microsoft.Reporting.WinForms.ReportParameter("prmEstado", locReport.Estado);
         if (locReport.CodigoProveedor == "") { Param[6] = new Microsoft.Reporting.WinForms.ReportParameter("prmCliProveedor", "Todos"); }
         else { Param[6] = new Microsoft.Reporting.WinForms.ReportParameter("prmCliProveedor", locReport.Nombre.Trim() + "(" + locReport.CodigoProveedor.Trim() + ")"); }

         if (locReport.MuestraTodosFactura == true) { Param[7] = new Microsoft.Reporting.WinForms.ReportParameter("prmFactura", "Todos"); }
         else {
            if (locReport.CodigoProveedor == "") {
               Param[7] = new Microsoft.Reporting.WinForms.ReportParameter("prmFactura","sin datos");
            }

            else { Param[7] = new Microsoft.Reporting.WinForms.ReportParameter("prmFactura", locReport.CodigoFact.ToString()); }
         }




         rv1.LocalReport.SetParameters(Param);
         rv1.LocalReport.EnableExternalImages = true;
         rv1.LocalReport.DataSources.Clear();
         rv1.LocalReport.DataSources.Add(rds);
         this.rv1.RefreshReport();
         rv1.ZoomPercent = 100;
      }

      private void rv1_Drillthrough(object sender, DrillthroughEventArgs e) {

         Isr = 0;
         RetIVA = 0;
         SubTotal = 0;
         IVA16 = 0;
         Desc = 0;
         SubTotalDesc = 0;
         IVA = 0;
         Total = 0;
         string ivapor = "";
         string NoPagoRec = "";
         ReportParameterInfoCollection DrillThroughValues = e.Report.GetParameters();

         foreach (ReportParameterInfo Rpi1 in DrillThroughValues) {
            if (Rpi1.Name == "prmFolio") {
               NoPagoRec = Rpi1.Values[0].ToString(); //.Trim().Substring(0, 2) + Rpi1.Values[0].ToString().Trim().Substring(3, 8) + Rpi1.Values[0].ToString().Trim().Substring(12, 2);
            }
         }

         LocalReport lr1 = (LocalReport)e.Report;
         var DatosEmpresa = cloReporte.ConsultaDatos();
         Microsoft.Reporting.WinForms.ReportParameter[] Param = new Microsoft.Reporting.WinForms.ReportParameter[11];
         string strVar;
         strVar = "C:\\STMonitor\\LogoBUS.jpg";
         if (System.IO.File.Exists(strVar)) {
            strVar = "file:" + strVar;
         }
         else {
            strVar = "C:\\STMonitor\\BlankSquare.jpg";
            if (System.IO.File.Exists(strVar)) {
               strVar = "file:" + strVar;
            }
            else {
               strVar = "a";
            }
         }
         DataTable dtCteInfo1 = cloReporte.SelectDTTickett(NoPagoRec);
         DataTable dtCteInfo = cloReporte.SelectDTPticketDet(NoPagoRec);
         foreach (DataRow table in dtCteInfo1.Rows) {
            SubTotal = cLogica.decValue(table[11]);
            Desc = cLogica.decValue(table[15]);
            IVA = cLogica.decValue(table[24]);

            if (cLogica.decValue(table[23]).ToString() == "8.0") {
               ivapor = cLogica.decValue(table[23]).ToString();
            }
            else {
               if (cLogica.decValue(table[23]).ToString() == "16.0") {
                  IVA16 = cLogica.decValue(table[23]);
               }
               else {
                  ivapor = "";
                  IVA16 = 0;
               }
            }
            RetIVA = Math.Abs(cLogica.decValue(table[30]));
            Isr = Math.Abs(cLogica.decValue(table[26]));
            Total = cLogica.decValue(table[14]);
         }
         SubTotalDesc = Total - Desc;

         Param[0] = new Microsoft.Reporting.WinForms.ReportParameter("prmPathLogo", strVar);
         Param[1] = new Microsoft.Reporting.WinForms.ReportParameter("prmFolio", NoPagoRec.Trim());
         Param[2] = new Microsoft.Reporting.WinForms.ReportParameter("prmEmpresaNombre", Comun.Sistema.sysNombreEmpresa);
         Param[3] = new Microsoft.Reporting.WinForms.ReportParameter("prmDireccion", "Detalle de Factura ");
         Param[4] = new Microsoft.Reporting.WinForms.ReportParameter("prmSubTotal", string.Format("{0:,00}", Total));
         Param[5] = new Microsoft.Reporting.WinForms.ReportParameter("prmDesc", string.Format("{0:,00}", Desc));
         Param[6] = new Microsoft.Reporting.WinForms.ReportParameter("prmSubTotalDesc", string.Format("{0:,00}", SubTotalDesc));
         if (ivapor != "") {
            Param[7] = new Microsoft.Reporting.WinForms.ReportParameter("prmIVA", string.Format("{0:,00}", IVA));
            Param[8] = new Microsoft.Reporting.WinForms.ReportParameter("prmIVA16",  "0.00");
         }
         else {
            Param[8] = new Microsoft.Reporting.WinForms.ReportParameter("prmIVA16", string.Format("{0:,00}", +IVA));
            Param[7] = new Microsoft.Reporting.WinForms.ReportParameter("prmIVA", "0.00");
         }
         Param[9] = new Microsoft.Reporting.WinForms.ReportParameter("prmTotal", string.Format("{0:,00}", IVA + SubTotalDesc));

         Param[10] = new Microsoft.Reporting.WinForms.ReportParameter("prmIva1", ivapor);

         

         lr1.ReportEmbeddedResource = "ConCompra.ReportDet.rdlc";
         Microsoft.Reporting.WinForms.ReportDataSource rds = new Microsoft.Reporting.WinForms.ReportDataSource();

         rds.Name = "DataSet1";
         rds.Value = dtCteInfo;
         lr1.EnableExternalImages = true;
         lr1.SetParameters(Param);
         lr1.DataSources.Clear();
         lr1.DataSources.Add(rds);
         lr1.Refresh();
      }
   }
}
