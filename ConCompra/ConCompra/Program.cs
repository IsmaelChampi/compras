﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConCompra {
   static class Program {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main() {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         ConCompra_Logica cloProject = new ConCompra_Logica();
         if (!cloProject.Main()) {

            return;
            // termina la ejecucion del programa
         }
         string dd = Comun.Sistema.sysDBBase;
         Comun.Sistema.sysDBInUse = Comun.Sistema.sysDBBase;
         switch (Comun.Sistema.sysOpCode) {
            case ConCompra_Comun.cnsConCompraPpal:
                Application.Run(new Form1());
               break;
            default:
               break;
         }
      }
   }
}
