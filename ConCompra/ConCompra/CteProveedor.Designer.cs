﻿namespace ConCompra {
   partial class CteProveedor {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if (disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.btnBuscar = new System.Windows.Forms.Button();
         this.mskNomBusCte = new CustomControls.MaskedTB();
         this.mskCodBusCte = new CustomControls.MaskedTB();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btnSiguiente = new System.Windows.Forms.Button();
         this.dgCliente = new System.Windows.Forms.DataGridView();
         this.btnCancelar = new System.Windows.Forms.Button();
         this.btnAceptar = new System.Windows.Forms.Button();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgCliente)).BeginInit();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.btnBuscar);
         this.groupBox1.Controls.Add(this.mskNomBusCte);
         this.groupBox1.Controls.Add(this.mskCodBusCte);
         this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.groupBox1.Location = new System.Drawing.Point(12, 12);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(712, 52);
         this.groupBox1.TabIndex = 13;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Buscar";
         // 
         // btnBuscar
         // 
         this.btnBuscar.BackColor = System.Drawing.SystemColors.Control;
         this.btnBuscar.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnBuscar.ForeColor = System.Drawing.Color.Black;
         this.btnBuscar.Location = new System.Drawing.Point(598, 13);
         this.btnBuscar.Name = "btnBuscar";
         this.btnBuscar.Size = new System.Drawing.Size(90, 29);
         this.btnBuscar.TabIndex = 2;
         this.btnBuscar.Text = "&Buscar";
         this.btnBuscar.UseVisualStyleBackColor = false;
         this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
         // 
         // mskNomBusCte
         // 
         this.mskNomBusCte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.mskNomBusCte.Location = new System.Drawing.Point(148, 18);
         this.mskNomBusCte.Mask = ">CCCCCCCCCCCCCCCCCCCCCCCCCCCCCC";
         this.mskNomBusCte.Name = "mskNomBusCte";
         this.mskNomBusCte.PromptChar = ' ';
         this.mskNomBusCte.Size = new System.Drawing.Size(436, 21);
         this.mskNomBusCte.TabIndex = 1;
         // 
         // mskCodBusCte
         // 
         this.mskCodBusCte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.mskCodBusCte.Location = new System.Drawing.Point(24, 17);
         this.mskCodBusCte.Mask = "00000000";
         this.mskCodBusCte.Name = "mskCodBusCte";
         this.mskCodBusCte.PromptChar = ' ';
         this.mskCodBusCte.Size = new System.Drawing.Size(108, 21);
         this.mskCodBusCte.TabIndex = 0;
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.btnSiguiente);
         this.groupBox2.Controls.Add(this.dgCliente);
         this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.groupBox2.Location = new System.Drawing.Point(12, 70);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(712, 427);
         this.groupBox2.TabIndex = 14;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Proveedores";
         // 
         // btnSiguiente
         // 
         this.btnSiguiente.BackColor = System.Drawing.SystemColors.Control;
         this.btnSiguiente.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnSiguiente.ForeColor = System.Drawing.Color.Black;
         this.btnSiguiente.Location = new System.Drawing.Point(598, 390);
         this.btnSiguiente.Name = "btnSiguiente";
         this.btnSiguiente.Size = new System.Drawing.Size(90, 27);
         this.btnSiguiente.TabIndex = 1;
         this.btnSiguiente.Text = "&>";
         this.btnSiguiente.UseVisualStyleBackColor = false;
         this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
         // 
         // dgCliente
         // 
         this.dgCliente.AllowUserToAddRows = false;
         this.dgCliente.AllowUserToDeleteRows = false;
         this.dgCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgCliente.Location = new System.Drawing.Point(24, 26);
         this.dgCliente.Name = "dgCliente";
         this.dgCliente.ReadOnly = true;
         this.dgCliente.RowHeadersWidth = 35;
         this.dgCliente.Size = new System.Drawing.Size(664, 354);
         this.dgCliente.TabIndex = 0;
         this.dgCliente.DoubleClick += new System.EventHandler(this.dgCliente_DoubleClick);
         // 
         // btnCancelar
         // 
         this.btnCancelar.BackColor = System.Drawing.SystemColors.Control;
         this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnCancelar.ForeColor = System.Drawing.Color.Black;
         this.btnCancelar.Location = new System.Drawing.Point(601, 509);
         this.btnCancelar.Name = "btnCancelar";
         this.btnCancelar.Size = new System.Drawing.Size(99, 36);
         this.btnCancelar.TabIndex = 20;
         this.btnCancelar.Text = "&Cancelar";
         this.btnCancelar.UseVisualStyleBackColor = false;
         this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
         // 
         // btnAceptar
         // 
         this.btnAceptar.BackColor = System.Drawing.SystemColors.Control;
         this.btnAceptar.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnAceptar.ForeColor = System.Drawing.Color.Black;
         this.btnAceptar.Location = new System.Drawing.Point(12, 510);
         this.btnAceptar.Name = "btnAceptar";
         this.btnAceptar.Size = new System.Drawing.Size(108, 36);
         this.btnAceptar.TabIndex = 19;
         this.btnAceptar.Text = "&Aceptar";
         this.btnAceptar.UseVisualStyleBackColor = false;
         this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
         // 
         // CteProveedor
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(749, 558);
         this.Controls.Add(this.btnCancelar);
         this.Controls.Add(this.btnAceptar);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.Name = "CteProveedor";
         this.Text = "Lista de Proveedores";
         this.Load += new System.EventHandler(this.CteProveedor_Load);
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.dgCliente)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button btnBuscar;
      private CustomControls.MaskedTB mskNomBusCte;
      private CustomControls.MaskedTB mskCodBusCte;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btnSiguiente;
      private System.Windows.Forms.DataGridView dgCliente;
      private System.Windows.Forms.Button btnCancelar;
      private System.Windows.Forms.Button btnAceptar;
   }
}