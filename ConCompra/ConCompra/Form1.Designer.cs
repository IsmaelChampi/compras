﻿namespace ConCompra {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if (disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.gbxProv = new System.Windows.Forms.GroupBox();
         this.mskNombre = new CustomControls.MaskedTB();
         this.mskCodigoProv = new CustomControls.MaskedTB();
         this.btnProveedor = new System.Windows.Forms.Button();
         this.btnValidaProv = new System.Windows.Forms.Button();
         this.ckBxTodos = new System.Windows.Forms.CheckBox();
         this.label35 = new System.Windows.Forms.Label();
         this.label36 = new System.Windows.Forms.Label();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.txtNumFactura = new CustomControls.MaskedTB();
         this.label1 = new System.Windows.Forms.Label();
         this.ckbTdasFacturas = new CustomControls.checkBoxNew();
         this.cmbEstado = new CustomControls.ComboBoxNew();
         this.label2 = new System.Windows.Forms.Label();
         this.maskFechaAl = new CustomControls.MaskedTB();
         this.mskFechaDel = new CustomControls.MaskedTB();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.btnCancelar = new System.Windows.Forms.Button();
         this.BtnGenerarReporte = new System.Windows.Forms.Button();
         this.gbxProv.SuspendLayout();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // gbxProv
         // 
         this.gbxProv.Controls.Add(this.mskNombre);
         this.gbxProv.Controls.Add(this.mskCodigoProv);
         this.gbxProv.Controls.Add(this.btnProveedor);
         this.gbxProv.Controls.Add(this.btnValidaProv);
         this.gbxProv.Controls.Add(this.ckBxTodos);
         this.gbxProv.Controls.Add(this.label35);
         this.gbxProv.Controls.Add(this.label36);
         this.gbxProv.Location = new System.Drawing.Point(12, 12);
         this.gbxProv.Name = "gbxProv";
         this.gbxProv.Size = new System.Drawing.Size(751, 78);
         this.gbxProv.TabIndex = 0;
         this.gbxProv.TabStop = false;
         this.gbxProv.Text = "Proveedores";
         // 
         // mskNombre
         // 
         this.mskNombre.Location = new System.Drawing.Point(234, 46);
         this.mskNombre.Name = "mskNombre";
         this.mskNombre.Size = new System.Drawing.Size(378, 20);
         this.mskNombre.TabIndex = 1;
         this.mskNombre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mskNombre_KeyDown);
         // 
         // mskCodigoProv
         // 
         this.mskCodigoProv.Location = new System.Drawing.Point(116, 46);
         this.mskCodigoProv.Name = "mskCodigoProv";
         this.mskCodigoProv.Size = new System.Drawing.Size(100, 20);
         this.mskCodigoProv.TabIndex = 0;
         this.mskCodigoProv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mskCodigoProv_KeyDown);
         // 
         // btnProveedor
         // 
         this.btnProveedor.BackColor = System.Drawing.Color.White;
         this.btnProveedor.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnProveedor.ForeColor = System.Drawing.Color.Black;
         this.btnProveedor.Location = new System.Drawing.Point(6, 39);
         this.btnProveedor.Name = "btnProveedor";
         this.btnProveedor.Size = new System.Drawing.Size(97, 27);
         this.btnProveedor.TabIndex = 3;
         this.btnProveedor.Text = "Proveedores";
         this.btnProveedor.UseVisualStyleBackColor = false;
         this.btnProveedor.Click += new System.EventHandler(this.btnProveedor_Click);
         // 
         // btnValidaProv
         // 
         this.btnValidaProv.BackColor = System.Drawing.Color.White;
         this.btnValidaProv.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnValidaProv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnValidaProv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnValidaProv.ForeColor = System.Drawing.Color.Black;
         this.btnValidaProv.Location = new System.Drawing.Point(639, 39);
         this.btnValidaProv.Name = "btnValidaProv";
         this.btnValidaProv.Size = new System.Drawing.Size(97, 27);
         this.btnValidaProv.TabIndex = 2;
         this.btnValidaProv.Text = "Vali&dar";
         this.btnValidaProv.UseVisualStyleBackColor = false;
         this.btnValidaProv.Click += new System.EventHandler(this.btnValidaProv_Click);
         // 
         // ckBxTodos
         // 
         this.ckBxTodos.AutoSize = true;
         this.ckBxTodos.Location = new System.Drawing.Point(28, 17);
         this.ckBxTodos.Name = "ckBxTodos";
         this.ckBxTodos.Size = new System.Drawing.Size(56, 17);
         this.ckBxTodos.TabIndex = 4;
         this.ckBxTodos.Text = "Todos";
         this.ckBxTodos.UseVisualStyleBackColor = true;
         this.ckBxTodos.CheckedChanged += new System.EventHandler(this.ckBxTodos_CheckedChanged);
         // 
         // label35
         // 
         this.label35.Location = new System.Drawing.Point(223, 21);
         this.label35.Name = "label35";
         this.label35.Size = new System.Drawing.Size(60, 23);
         this.label35.TabIndex = 4;
         this.label35.Text = "Nombre";
         this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // label36
         // 
         this.label36.Location = new System.Drawing.Point(113, 21);
         this.label36.Name = "label36";
         this.label36.Size = new System.Drawing.Size(52, 23);
         this.label36.TabIndex = 5;
         this.label36.Text = "Código";
         this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.txtNumFactura);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.ckbTdasFacturas);
         this.groupBox1.Location = new System.Drawing.Point(12, 141);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(307, 74);
         this.groupBox1.TabIndex = 3;
         this.groupBox1.TabStop = false;
         // 
         // txtNumFactura
         // 
         this.txtNumFactura.Location = new System.Drawing.Point(109, 40);
         this.txtNumFactura.Name = "txtNumFactura";
         this.txtNumFactura.Size = new System.Drawing.Size(162, 20);
         this.txtNumFactura.TabIndex = 37;
         // 
         // label1
         // 
         this.label1.Location = new System.Drawing.Point(6, 37);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(97, 23);
         this.label1.TabIndex = 8;
         this.label1.Text = "Número Factura :";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // ckbTdasFacturas
         // 
         this.ckbTdasFacturas.AutoSize = true;
         this.ckbTdasFacturas.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.ckbTdasFacturas.Location = new System.Drawing.Point(9, 17);
         this.ckbTdasFacturas.Name = "ckbTdasFacturas";
         this.ckbTdasFacturas.Size = new System.Drawing.Size(116, 17);
         this.ckbTdasFacturas.TabIndex = 0;
         this.ckbTdasFacturas.Text = "Todas las Facturas";
         this.ckbTdasFacturas.UseVisualStyleBackColor = true;
         this.ckbTdasFacturas.CheckedChanged += new System.EventHandler(this.ckbTdasFacturas_CheckedChanged);
         // 
         // cmbEstado
         // 
         this.cmbEstado.FormattingEnabled = true;
         this.cmbEstado.Location = new System.Drawing.Point(121, 221);
         this.cmbEstado.Name = "cmbEstado";
         this.cmbEstado.Size = new System.Drawing.Size(121, 21);
         this.cmbEstado.TabIndex = 4;
         // 
         // label2
         // 
         this.label2.Location = new System.Drawing.Point(18, 219);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(97, 23);
         this.label2.TabIndex = 9;
         this.label2.Text = "Estado :";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // maskFechaAl
         // 
         this.maskFechaAl.Location = new System.Drawing.Point(264, 105);
         this.maskFechaAl.Mask = "00/00/0000";
         this.maskFechaAl.Name = "maskFechaAl";
         this.maskFechaAl.PromptChar = ' ';
         this.maskFechaAl.Size = new System.Drawing.Size(100, 20);
         this.maskFechaAl.TabIndex = 2;
         this.maskFechaAl.ValidatingType = typeof(System.DateTime);
         this.maskFechaAl.Leave += new System.EventHandler(this.maskFechaAl_Leave);
         // 
         // mskFechaDel
         // 
         this.mskFechaDel.Location = new System.Drawing.Point(127, 105);
         this.mskFechaDel.Mask = "00/00/0000";
         this.mskFechaDel.Name = "mskFechaDel";
         this.mskFechaDel.PromptChar = ' ';
         this.mskFechaDel.Size = new System.Drawing.Size(100, 20);
         this.mskFechaDel.TabIndex = 1;
         this.mskFechaDel.ValidatingType = typeof(System.DateTime);
         this.mskFechaDel.Leave += new System.EventHandler(this.mskFechaDel_Leave);
         // 
         // label3
         // 
         this.label3.Location = new System.Drawing.Point(24, 104);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(97, 24);
         this.label3.TabIndex = 35;
         this.label3.Text = "Del:";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // label4
         // 
         this.label4.Location = new System.Drawing.Point(235, 105);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(23, 23);
         this.label4.TabIndex = 34;
         this.label4.Text = "Al:";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         // 
         // btnCancelar
         // 
         this.btnCancelar.BackColor = System.Drawing.Color.White;
         this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.btnCancelar.ForeColor = System.Drawing.Color.Black;
         this.btnCancelar.Location = new System.Drawing.Point(666, 259);
         this.btnCancelar.Name = "btnCancelar";
         this.btnCancelar.Size = new System.Drawing.Size(97, 28);
         this.btnCancelar.TabIndex = 6;
         this.btnCancelar.Text = "&Cancelar";
         this.btnCancelar.UseVisualStyleBackColor = false;
         this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
         // 
         // BtnGenerarReporte
         // 
         this.BtnGenerarReporte.BackColor = System.Drawing.Color.White;
         this.BtnGenerarReporte.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
         this.BtnGenerarReporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
         this.BtnGenerarReporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.BtnGenerarReporte.ForeColor = System.Drawing.Color.Black;
         this.BtnGenerarReporte.Location = new System.Drawing.Point(12, 259);
         this.BtnGenerarReporte.Name = "BtnGenerarReporte";
         this.BtnGenerarReporte.Size = new System.Drawing.Size(195, 28);
         this.BtnGenerarReporte.TabIndex = 5;
         this.BtnGenerarReporte.Text = "&Generar Reporte";
         this.BtnGenerarReporte.UseVisualStyleBackColor = false;
         this.BtnGenerarReporte.Click += new System.EventHandler(this.BtnGenerarReporte_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.ClientSize = new System.Drawing.Size(783, 309);
         this.Controls.Add(this.btnCancelar);
         this.Controls.Add(this.BtnGenerarReporte);
         this.Controls.Add(this.maskFechaAl);
         this.Controls.Add(this.mskFechaDel);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.cmbEstado);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.gbxProv);
         this.Name = "Form1";
         this.Text = "Compra Proveedor";
         this.Load += new System.EventHandler(this.Form1_Load);
         this.gbxProv.ResumeLayout(false);
         this.gbxProv.PerformLayout();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.GroupBox gbxProv;
      private System.Windows.Forms.Button btnProveedor;
      private System.Windows.Forms.Button btnValidaProv;
      private System.Windows.Forms.CheckBox ckBxTodos;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.Label label36;
      private CustomControls.MaskedTB mskNombre;
      private CustomControls.MaskedTB mskCodigoProv;
      private System.Windows.Forms.GroupBox groupBox1;
      private CustomControls.checkBoxNew ckbTdasFacturas;
      private System.Windows.Forms.Label label1;
      private CustomControls.ComboBoxNew cmbEstado;
      private System.Windows.Forms.Label label2;
      private CustomControls.MaskedTB maskFechaAl;
      private CustomControls.MaskedTB mskFechaDel;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button btnCancelar;
      private System.Windows.Forms.Button BtnGenerarReporte;
      private CustomControls.MaskedTB txtNumFactura;
   }
}

