﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConCompra {
   public partial class CteProveedor : Form {
      Logica.Logica cloLogica = new Logica.Logica();
      ConCompra.ClienteConsulta ccoCteConsulta = new ConCompra.ClienteConsulta();
      ConCompra_Logica proLogica = new ConCompra_Logica();
      string strProgramName = "Consulta Proveedor";

      public CteProveedor() {
         InitializeComponent();
      }

      bool ExisteError() {
         bool blnReturn = false;
         if (Comun.Sistema.sysError != 0) {
            MessageBox.Show(Comun.Sistema.sysErrorDescr, strProgramName);
            Comun.Sistema.sysErrorDescr = "";
            Comun.Sistema.sysError = 0;
            Environment.Exit(999);
            blnReturn = true;
         }

         if (Comun.Sistema.sysMensaje != "") {
            MessageBox.Show(Comun.Sistema.sysMensaje, strProgramName);
            Comun.Sistema.sysErrorDescr = "";
            Comun.Sistema.sysError = 0;
            Comun.Sistema.sysMensaje = "";
            blnReturn = true;
         }
         return blnReturn;
      }

      private void SetDGProperties() {
         dgCliente.ColumnCount = 2;
         dgCliente.ColumnHeadersVisible = true;
         DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();
         columnHeaderStyle.BackColor = Color.Black;
         dgCliente.ColumnHeadersDefaultCellStyle = columnHeaderStyle;
         dgCliente.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
         dgCliente.Columns[0].Name = "CodigoProv"; dgCliente.Columns[1].Name = "Nombre";
         dgCliente.Columns[0].Width = 95;
         dgCliente.Columns[1].Width = 510;
         dgCliente.EnableHeadersVisualStyles = false;
         dgCliente.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(230, 232, 234);
         dgCliente.RowHeadersDefaultCellStyle.BackColor = Color.FromArgb(230, 232, 234);
      }

      private void CteProveedor_Load(object sender, EventArgs e) {
         ccoCteConsulta.UltimoValorUsado = "";
         ccoCteConsulta.NombreLLave = "Nombre";
         ccoCteConsulta.SignoComparacion = "";
         ccoCteConsulta.NoRengAMostrar = 15;
         ccoCteConsulta.CodigoCte = "";
         ccoCteConsulta.NombreCte = "";
         SetDGProperties();
         DisplayPage();
      }

      private void DisplayPage() {

         if (mskCodBusCte.Text != "") {
            ccoCteConsulta.NombreLLave = "CodigoProv";
            ccoCteConsulta.CodigoCte = mskCodBusCte.Text;
            ccoCteConsulta.NombreCte = "";
         }
         if (mskNomBusCte.Text != "") {
            ccoCteConsulta.NombreLLave = "Nombre";
            ccoCteConsulta.CodigoCte = mskNomBusCte.Text;
            ccoCteConsulta.CodigoCte = "";
         }
         Comun.ProveedorCN ccoClienteCN = new Comun.ProveedorCN();
         ccoClienteCN.CodigoCte = mskCodBusCte.Text;
         ccoClienteCN.NombreCte = mskNomBusCte.Text;
         ccoClienteCN.NoRengAmostrar = ccoCteConsulta.NoRengAMostrar;
         ccoClienteCN.SignoComparacion = ">=";
         var lstProv = proLogica.BuscaProvCont(ccoClienteCN); if (ExisteError()) { return; }

         if (lstProv.Count <= 0) {
            switch (ccoCteConsulta.NombreLLave) {
               case "CodigoProv":
                  MessageBox.Show("Código de Proveedor no encontrado");
                  mskCodBusCte.Focus();
                  return;
               case "Nombre":
                  MessageBox.Show("Código de Proveedor no encontrado");
                  mskCodBusCte.Focus();
                  return;
            }
         }
         bool blnPrimerRenglon = true;
         foreach (var item in lstProv) {
            if (blnPrimerRenglon) {
               mskCodBusCte.Text = item.CodigoCte;
               mskNomBusCte.Text = item.NombreCte;
               blnPrimerRenglon = false;
            }
            dgCliente.Rows.Add();
            dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["CodigoProv"].Value = item.CodigoCte;
            dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["Nombre"].Value = item.NombreCte;

         }
         switch (ccoCteConsulta.NombreLLave) {
            case "CodigoProv":
               ccoCteConsulta.UltimoValorUsado = dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["CodigoProv"].Value.ToString();
               break;
            case "Nombre":
               ccoCteConsulta.UltimoValorUsado = dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["Nombre"].Value.ToString();
               break;
         }


      }

      private void btnBuscar_Click(object sender, EventArgs e) {
         if (mskCodBusCte.Text.Trim() == "" && mskNomBusCte.Text.Trim() == "") {
            MessageBox.Show("Debe registrar Código o Nombre de Búsqueda", strProgramName);
            return;
         }
         if (mskCodBusCte.Text.Trim() != "" && mskNomBusCte.Text.Trim() != "") {
            return;
         }
         dgCliente.Rows.Clear();
         DisplayPage();
      }

      private void btnAceptar_Click(object sender, EventArgs e) {
         if (mskCodBusCte.Text != "" && mskNomBusCte.Text != "") {
            ConCompra_Comun.strCodigoCte = dgCliente.CurrentRow.Cells["CodigoProv"].Value.ToString();
            ConCompra_Comun.strNombreCte = dgCliente.CurrentRow.Cells["Nombre"].Value.ToString();
            this.Close();
         }
      }

      private void btnCancelar_Click(object sender, EventArgs e) {
         this.Close();
      }

      private void btnSiguiente_Click(object sender, EventArgs e) {
         Comun.ProveedorCN ccoCteCN = new Comun.ProveedorCN();
         switch (ccoCteConsulta.NombreLLave) {
            case "CodigoProv":
               ccoCteCN.CodigoCte = ccoCteConsulta.UltimoValorUsado;
               break;
            case "Nombre":
               ccoCteCN.NombreCte = ccoCteConsulta.UltimoValorUsado;
               break;
         }
         ccoCteConsulta.SignoComparacion = ">";
         ccoCteCN.SignoComparacion = ccoCteConsulta.SignoComparacion;
         ccoCteCN.NoRengAmostrar = ccoCteConsulta.NoRengAMostrar;
         List<Comun.ProveedorCN> lstClienteCN = cloLogica.SelectProveedoresCN(ccoCteCN);
         foreach (var item in lstClienteCN) {
            dgCliente.Rows.Add();
            dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["CodigoProv"].Value = item.CodigoCte;
            dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["Nombre"].Value = item.NombreCte;
         }
         switch (ccoCteConsulta.NombreLLave) {
            case "CodigoProv":
               ccoCteConsulta.UltimoValorUsado = dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["CodigoProv"].Value.ToString();
               break;
            case "Nombre":
               ccoCteConsulta.UltimoValorUsado = dgCliente.Rows[dgCliente.Rows.Count - 1].Cells["Nombre"].Value.ToString();
               break;
         }
         dgCliente.CurrentCell = dgCliente.Rows[dgCliente.Rows.Count - 1].Cells[0];
      }

      private void dgCliente_DoubleClick(object sender, EventArgs e) {
         btnAceptar_Click(sender,e);
      }
   }
}
