﻿namespace ConCompra {
   partial class ReporteCnCpra {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if (disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.rv1 = new Microsoft.Reporting.WinForms.ReportViewer();
         this.SuspendLayout();
         // 
         // rv1
         // 
         this.rv1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.rv1.Location = new System.Drawing.Point(0, 0);
         this.rv1.Name = "rv1";
         this.rv1.Size = new System.Drawing.Size(803, 532);
         this.rv1.TabIndex = 0;
         this.rv1.Drillthrough += new Microsoft.Reporting.WinForms.DrillthroughEventHandler(this.rv1_Drillthrough);
         // 
         // ReporteCnCpra
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(803, 532);
         this.Controls.Add(this.rv1);
         this.Name = "ReporteCnCpra";
         this.Text = "ReporteCnCpra";
         this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
         this.Load += new System.EventHandler(this.ReporteCnCpra_Load);
         this.ResumeLayout(false);

      }

      #endregion

      private Microsoft.Reporting.WinForms.ReportViewer rv1;
   }
}