﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConCompra {
   public partial class Form1 : Form {
      public Comun.Proveedor ccoProveedor = new Comun.Proveedor();
      public Logica.Logica cloLogica = new Logica.Logica();
      string strProgramName = "";
      DateTime fechaAñoDel;
      DateTime fechaAñoAL;
      int añoDel;
      int añoAl;
      public bool muestra;

      public Form1() {
         InitializeComponent();
      }

      public string completaFecha(string strFechaAcompletar) {
         string strFecha = strFechaAcompletar.Replace("/", "").Trim();
         DateTime FechaHoraServer = cloLogica.GetServerDateTime();
         if (strFecha.Trim() == "") { return FechaHoraServer.ToString(); }

         switch (strFecha.Length) {
            case 0:
            case 1:
            case 2:
               if (Convert.ToInt32(strFecha) >= DateTime.DaysInMonth(FechaHoraServer.Year, FechaHoraServer.Month)) { return strFechaAcompletar; }
               strFecha = strFecha.Trim().PadLeft(2, '0') + "/" + FechaHoraServer.Month.ToString().Trim().PadLeft(2, '0') + "/" + FechaHoraServer.Year;
               break;
            case 3:
            case 4:
               strFecha = strFecha.Substring(0, 2) + "/" + strFecha.Substring(2).Trim().PadLeft(2, '0') + "/" + FechaHoraServer.Year;
               break;
            default:
               string strFecha1 = strFecha.Substring(0, 2) + "/" + strFecha.Substring(2, 2).Trim();
               strFecha = strFecha1 + "/" + FechaHoraServer.Year.ToString().Substring(0, 4 - (strFecha.Length - 4)) + strFecha.Substring(4);
               break;
         }

         if (IsDate(strFecha)) { return strFecha; }
         else { return strFechaAcompletar; }
      }

      private bool IsDate(string inputDate) {
         bool isDate = true;
         try { DateTime dt = DateTime.Parse(inputDate); }
         catch { isDate = false; }
         return isDate;
      }

      private void Form1_Load(object sender, EventArgs e) {
         string strVar = "";
         mskFechaDel.Text = completaFecha("01");
         maskFechaAl_Leave(sender, e);
         strVar = ""; strVar = "1¬Activo,2¬Terminado"; cmbEstado.LoadWithList(strVar);
      }

      private void ckBxTodos_CheckedChanged(object sender, EventArgs e) {
         if (ckBxTodos.Checked == true) {
            muestra = true;
            btnProveedor.Enabled = false;
            mskCodigoProv.Enabled = false;
            mskNombre.Enabled = false;
            btnValidaProv.Enabled = false;
            mskCodigoProv.Text = "";
            mskNombre.Text = "";
         }
         else {
            muestra = false;
            btnProveedor.Enabled = true;
            mskCodigoProv.Enabled = true;
            mskNombre.Enabled = true;
            btnValidaProv.Enabled = true;
         }
      }

      private void btnProveedor_Click(object sender, EventArgs e) {
         CteProveedor frmConProv = new CteProveedor();
         mskCodigoProv.Text = "";
         mskNombre.Text = "";
         frmConProv.ShowDialog();
         if (ConCompra_Comun.strCodigoCte != "") {
            mskCodigoProv.Text = ConCompra_Comun.strCodigoCte;
            mskNombre.Text = ConCompra_Comun.strNombreCte;
            btnValidaProv_Click(sender, e);
         }
      }

      bool ExisteError() {
         bool blnReturn = false;
         if (Comun.Sistema.sysError != 0) {
            MessageBox.Show(Comun.Sistema.sysErrorDescr, strProgramName);
            Comun.Sistema.sysErrorDescr = "";
            Comun.Sistema.sysError = 0;
            Environment.Exit(99);
            blnReturn = true;
         }

         if (Comun.Sistema.sysMensaje != "") {
            MessageBox.Show(Comun.Sistema.sysMensaje, strProgramName);
            Comun.Sistema.sysErrorDescr = "";
            Comun.Sistema.sysError = 0;
            Comun.Sistema.sysMensaje = "";
            blnReturn = true;
         }
         return blnReturn;
      }

      private void btnValidaProv_Click(object sender, EventArgs e) {
         if (mskCodigoProv.Text.Trim() == "" && mskNombre.Text == "") {
            MessageBox.Show("Debe registrar Código o Nombre de Proveedor", strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            mskCodigoProv.Focus();
            return;
         }
         ccoProveedor = new Comun.Proveedor();
         List<Comun.Proveedor> consulta = new List<Comun.Proveedor>();

         if (mskCodigoProv.Text.Trim() != "") {
            ccoProveedor.CodigoProv = mskCodigoProv.Text.PadLeft(8, ' ');
            ccoProveedor.Nombre = "";
            consulta = cloLogica.SelectProveedor(ccoProveedor, "=", 1); if (ExisteError()) { this.Close(); return; }
            if (consulta.Count == 0) {
               MessageBox.Show("Proveedor No Registrado", strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Information);
               mskCodigoProv.Focus();
               return;
            }
         }

         if (mskNombre.Text != "") {
            ccoProveedor.CodigoProv = "";
            ccoProveedor.Nombre = mskNombre.Text;
            consulta = cloLogica.SelectProveedor(ccoProveedor, ">=", 1); if (ExisteError()) { this.Close(); return; }
            if (consulta.Count == 0) {
               MessageBox.Show("Proveedor no registrado", strProgramName);
               mskCodigoProv.Focus();
               return;
            }
         }

         ccoProveedor = consulta[0];


         mskCodigoProv.Text = ccoProveedor.CodigoProv;
         mskNombre.Text = ccoProveedor.Nombre;
      }

      private void maskFechaAl_Leave(object sender, EventArgs e) {
         maskFechaAl.Text = completaFecha(maskFechaAl.Text);
      }

      private void mskFechaDel_Leave(object sender, EventArgs e) {
         mskFechaDel.Text = completaFecha(mskFechaDel.Text);
      }

      private void mskCodigoProv_KeyDown(object sender, KeyEventArgs e) {
         if (e.KeyValue != (char)Keys.Enter && e.KeyValue != 18 && e.KeyValue != (char)Keys.Tab) {
            mskNombre.Text = "";
            
         }
      }

      private void mskNombre_KeyDown(object sender, KeyEventArgs e) {
         if (e.KeyValue != (char)Keys.Enter && e.KeyValue != 18 && e.KeyValue != (char)Keys.Tab) {
            mskCodigoProv.Text = "";
         }
      }

      private void ckbTdasFacturas_CheckedChanged(object sender, EventArgs e) {
         if (ckbTdasFacturas.Checked == true) {
            txtNumFactura.Enabled = false;
            txtNumFactura.Text = "";
         }
         else {
            txtNumFactura.Enabled = true;
         }
      }

      private void BtnGenerarReporte_Click(object sender, EventArgs e) {
         if (!ValidaDatos(sender, e)) { return; }
         ConCompra.reporte datosRepor = new ConCompra.reporte();
         datosRepor.Estado = cmbEstado.Text;
         datosRepor.TodosProveedor = ckBxTodos.Checked;
         datosRepor.MuestraTodosFactura = ckbTdasFacturas.Checked;
         datosRepor.CodigoFact = txtNumFactura.Text;
         datosRepor.CodigoProveedor = mskCodigoProv.Text;
         datosRepor.FechaDel = Convert.ToDateTime(mskFechaDel.Text + " 00:00:00");
         datosRepor.FechaAl = Convert.ToDateTime(maskFechaAl.Text + " 23:59:59");
         datosRepor.Nombre = mskNombre.Text;
       
         //datosRepor.OrdenVendedor = CbxAgrupado.Checked;
         ReporteCnCpra reporte = new ReporteCnCpra();
         reporte.locReport = datosRepor;
         reporte.ShowDialog();
      }

      private bool ValidaDatos(object sender, EventArgs e) {
         bool blnReturn = false;
         fechaAñoDel = Convert.ToDateTime(mskFechaDel.Text);
         fechaAñoAL = Convert.ToDateTime(maskFechaAl.Text);
         añoDel = fechaAñoDel.Year;
         añoAl = fechaAñoAL.Year;
         if (añoDel < añoAl || añoDel > añoAl) {
            maskFechaAl.Text = "";
            MessageBox.Show("El año en las fechas Del y Al tiene que ser el mismo", strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            mskFechaDel.Text = completaFecha("01");
            maskFechaAl_Leave(sender, e);
            mskFechaDel.Focus();
            return blnReturn;
         }
         else if (añoDel == añoAl) {
            if (muestra == false && mskCodigoProv.Text == "" && mskNombre.Text == "") {
               MessageBox.Show("No se a registrado los datos del cliente", strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
               mskCodigoProv.Focus();
               return blnReturn;
            }
            if (fechaAñoDel > fechaAñoAL) {
               maskFechaAl.Text = "";
               MessageBox.Show("La fecha del debe ser menor a la fecha al", strProgramName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
               maskFechaAl_Leave(sender, e);
               mskFechaDel.Focus();
               return blnReturn;
            }

         }
         blnReturn = true;
         return blnReturn;
      }

      private void btnCancelar_Click(object sender, EventArgs e) {
         this.Close();
      }
   }
}
