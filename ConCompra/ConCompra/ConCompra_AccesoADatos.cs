﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConCompra {
  public class ConCompra_AccesoADatos {
     Logica.Logica loc = new Logica.Logica();
     public SqlConnection cone = new SqlConnection();
     AccesoADatos.AccesoADatos cadAccesoADatos = new AccesoADatos.AccesoADatos();
     public DataSet ds = new DataSet();

     public string strCadena(string strDBName) {

        string mensaje = "";
        //string NameRuta = @"C:\STMONITOR";
        //string NameFill = @"C:\STMONITOR\Config.txt";
        //Comun.Sistema.sysServer = "WIN7-PC"; // AAJJAA

        if (Comun.Sistema.sysTipoBD == "2005") { return cnStringStec(strDBName); }

        //if (!Directory.Exists(NameRuta)) { mensaje = "La ruta a la cual quiere acceder no se encuentra"; }
        //else {
        // StreamReader strMaquina = new StreamReader(NameFill);
        //mensaje = strMaquina.ReadLine();
        //           mensaje = @"Data Source=" + mensaje + @"; Initial Catalog=" + Comun.Sistema.sysDBInUse + "; MultipleActiveResultSets=True; User ID=sa; Password=siatecmexico";
        string strVar = Comun.Sistema.strPathEmpresa; //Comun.Sistema.sysServer + string.Format(@"G{0}\E{1}\", Comun.Sistema.sysCodigoGrupo, Comun.Sistema.sysCodigoEmpresa) + strDataBaseName;
        if (strDBName.ToUpper() == "SISTEMA") { strVar = Comun.Sistema.strPathSistema; }

        //mensaje = @"Data Source=" + mensaje + @"; Initial Catalog=" + strDBName + "; MultipleActiveResultSets=True; User ID=sa; Password=siatecmexico";
        mensaje = @"Data Source={0}\SQLSERVER10; Initial Catalog={1}{2}.mdf; MultipleActiveResultSets=True; User ID=sa; Password=siatecMexico2014=)(";
        mensaje = @"Data Source={0}; Initial Catalog={1}{2}; MultipleActiveResultSets=True; User ID=sa; Password=siatecMexico2014=)(";
        // AAJJAA SE QUITO .mdf del nombre del Initial Catalog
        mensaje = string.Format(mensaje, Comun.Sistema.sysServer, strVar, strDBName);
        //}
        return mensaje;

        //StreamReader strMaquina = null;
        //string mensaje = "", nameBD = "";
        //string NameRuta = @"C:\STMONITOR";
        //string NameFill = @"C:\STMONITOR\Config.txt";
        //if (!Directory.Exists(NameRuta)) { mensaje = "La ruta a la cual quiere acceder no se encuentra"; }
        //else {
        //   strMaquina = new StreamReader(NameFill);
        //   mensaje = strMaquina.ReadLine();
        //   nameBD = strMaquina.ReadLine();
        //   mensaje = @"Data Source=" + mensaje + "; Initial Catalog=" + nameBD + "; MultipleActiveResultSets=True; User ID=sa; Password=siatecMexico2014=)(";
        //}
        //strMaquina.Close();
        //return mensaje;

        //StreamReader strMaquina = null;
        //string mensaje = "", nameBD = "";
        //string NameRuta = @"C:\STMONITOR";
        //string NameFill = @"C:\STMONITOR\Config.txt";
        //if (!Directory.Exists(NameRuta)) { mensaje = "La ruta a la cual quiere acceder no se encuentra"; }
        //else {
        //   strMaquina = new StreamReader(NameFill);
        //   mensaje = strMaquina.ReadLine();
        //   nameBD = strMaquina.ReadLine();
        //   mensaje = @"Data Source=" + mensaje + "; Initial Catalog=" + nameBD + "; MultipleActiveResultSets=True; User ID=sa; Password=siatecMexico2014=)(";
        //}
        //strMaquina.Close();
        //return mensaje;
     }

     public string cnStringStec(string strDBName) {
        string strVar = Comun.Sistema.strPathEmpresa; //Comun.Sistema.sysServer + string.Format(@"G{0}\E{1}\", Comun.Sistema.sysCodigoGrupo, Comun.Sistema.sysCodigoEmpresa) + strDataBaseName;
        //strVar = @"C:\Siatec\SiatecInfo\G1\E9\";
        if (strDBName == "SISTEMA") { strVar = Comun.Sistema.strPathSistema; }
        string cnString = "Data Source={0}\\SQLEXPRESS;Database={1}{2}.mdf;" +
         Comun.Methods.Rot39(@"|L>K pkdL:bw:LLPHK=dLB:M><YWW\d)(b") + "MultipleActiveResultsets=yes";

        if (Comun.Sistema.sysNombreEmpresa.IndexOf("CRP DE MEXICO") >= 0) {
           cnString = "Data Source={0}\\SQLEXPRESS;Database={1}{2}.mdf;" +
           "User ID=sa;Password=siatec2005;" + "MultipleActiveResultsets=yes";
        }
        cnString = string.Format(cnString, Comun.Sistema.sysServer, strVar, strDBName);
        //         cnString = @"Data Source={0}\SQLEXPRESS;Database={1}.mdf;User ID=sa;Password=siatec2005=)(;MultipleActiveResultsets=yes";
        //         cnString = string.Format(cnString, Comun.Sistema.sysServer.ToString(), strVar);
        return cnString;
     }

     public SqlDataReader SelectDR1(string DBName, string strSQL, string[] arrParam, CommandBehavior cmdBhvr) {
        SqlDataReader dr = null;
        Comun.Sistema.sysError = 0;
        Comun.Sistema.sysErrorDescr = "";
        Comun.Sistema.sysErrorDescr = "";

        try {
           if (cone.State != ConnectionState.Open) { cone.ConnectionString = cadAccesoADatos.cnString("Sistema"); cone.Open(); }
           SqlCommand comando = new SqlCommand(strSQL);
           for (int i = 1; i <= Convert.ToInt32(arrParam[0]); i++) {
              string strParam = "@P" + i.ToString().PadLeft(2, '0');
              comando.Parameters.AddWithValue(strParam, arrParam[i]);
           }
           comando.Connection = cone;
           dr = comando.ExecuteReader(cmdBhvr | CommandBehavior.CloseConnection);
        }
        catch (System.Data.SqlClient.SqlException ex) {
           if (ex.Number == 0) { Comun.Sistema.sysError = -99999;/*error no codificado*/}
           else { Comun.Sistema.sysError = ex.Number; Comun.Sistema.sysErrorDescr = ex.Message + " " + cone.ConnectionString; }

           if (ex.Number == -1)//No se pudo conectar a la Base de Datos. Avise a su administrador
                {
              Comun.Sistema.sysError = -1;
              Comun.Sistema.sysErrorDescr = "Error en la conexión a la Base de datos. Avise a su Administrador";
              cadAccesoADatos.RegistroBitacora2(ex.Source, ex.TargetSite.DeclaringType.ToString(), ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" en ")), ex.Number, ex.Message + " " + cone.ConnectionString);
              //se manda mensaje a usuario en capa de presentaCION 
           }
           else {
              Comun.Sistema.sysError = ex.Number;
              Comun.Sistema.sysErrorDescr = "Ha ocurrido un error. Avise a su Administrador (" + (ex.Message) + ")";
              cadAccesoADatos.RegistroBitacora2(ex.Source, ex.TargetSite.DeclaringType.ToString(), ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" en ")), ex.Number, ex.Message + " " + cone.ConnectionString);
           }
        }
        catch (Exception ex) {
           cadAccesoADatos.RegistroBitacora2(ex.Source, ex.TargetSite.DeclaringType.ToString(), ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" en ")), ex.HResult, ex.Message + " " + cone.ConnectionString);
           Comun.Sistema.sysError = ex.HResult;
           Comun.Sistema.sysErrorDescr = "Ha ocurrido un error. Avise a su Administrador (" + (ex.Message) + ")";
        }
        return dr;
     }

     public void RegistroBitacora2(string Programa, string Metodo, string Linea, int NoError, string DescripcionError) {
        StreamWriter scrip;
        if (File.Exists(@"C:\STMONITOR\Bitacora.err")) {
           //MessageBox.Show("Archivo existente");
        }
        else {
           scrip = File.CreateText(@"C:\STMONITOR\Bitacora.err");
           scrip.Close();
        }
        scrip = File.AppendText(@"C:\STMONITOR\Bitacora.err");
        String strFecha = string.Format("{0:yyyy/MM/dd}", DateTime.Now);
        String strHora = string.Format("{0:hh:mm:ss}", DateTime.Now);
        String strPrograma = Programa;
        String strMetodo = Metodo;
        //String strLink = link;
        String strLinea = Linea;
        Int32 intNoError = NoError;
        String strDescripcionError = DescripcionError;
        scrip.WriteLine(strFecha.ToString() + " " + strHora.ToString() + " " + strPrograma.ToString() + " " + strMetodo.ToString() + " " + strLinea.ToString() + " " + intNoError.ToString() + " " + strDescripcionError.ToString());
        scrip.Flush();
        scrip.Close();
     }

     public SqlDataReader SelectDR(string strSQL, string[] arrParam) {
        SqlDataReader dr = null;
        try {
           if (cone.State != ConnectionState.Open) { cone.ConnectionString = strCadena("STMonitor"); cone.Open(); }
           SqlCommand comando = new SqlCommand(strSQL);
           for (int i = 1; i <= Convert.ToInt32(arrParam[0]); i++) {
              string strParam = "@P" + i.ToString().PadLeft(2, '0');
              comando.Parameters.AddWithValue(strParam, arrParam[i]);
           }
           comando.Connection = cone;
           dr = comando.ExecuteReader();
        }
        catch (System.Data.SqlClient.SqlException ex) {
           MessageBox.Show(ex.Message);
           if (ex.ErrorCode == 0) { sistema.SysError = -99999; sistema.SysErrorDesc = ex.Message;/*error no codificado*/}
           else { sistema.SysError = ex.ErrorCode; sistema.SysErrorDesc = ex.Message; }

           if (ex.ErrorCode == -2146232060)//No se pudo conectar a la Base de Datos. Avise a su administrador
                {//se manda mensaje a usuario en capa de presentaCION 
           }
           else {
              RegistroBitacora2(ex.StackTrace, ex.TargetSite.DeclaringType.ToString(), ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" en ")), ex.Number, ex.Message + " " + cone.State + " " + strSQL + " " + arrParam[0] + " " + arrParam[1]);
           }
        }
        catch (Exception ex) {

           MessageBox.Show(ex.Message);
           RegistroBitacora2("Info. Monitoreo Cliente", ex.TargetSite.DeclaringType.ToString(), ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(" en ")), ex.HResult, ex.Message + " " + cone.State + " " + strSQL + " " + arrParam[0] + " " + arrParam[1]);
           sistema.SysError = ex.HResult; sistema.SysErrorDesc = ex.Message;
        }
        return dr;
     }

     public List<Comun.ProveedorCN> BuscaProv(Comun.ProveedorCN ccCont) {//Se hizo un cambio agregando el Codigo de partición en la consulta MGB
        string[] arrParam = new string[1];
        string strSQL = "";
        strSQL = "SELECT CodigoProveedor,Nombre FROM Proveedor ";
     
        strSQL = string.Format(strSQL);

        SqlDataReader dr = SelectDR(strSQL, arrParam);
        var resultado = new List<Comun.ProveedorCN>();
        while (dr.Read()) {
           Comun.ProveedorCN ccConts = new Comun.ProveedorCN();
      
           ccConts.CodigoCte = dr["CodigoProveedor"].ToString();
           ccConts.NombreCte = dr["Nombre"].ToString();
          
           resultado.Add(ccConts);
        }
        cone.Close();
        return resultado;
     }

     public DataTable SelectDTProveedor(reporte ccReport) {
        bool EstadoCtivo = false;
        string strSQL = "Select CodigoProveedor,NoFacturaCpa,tIPO,FechaFacturaCpa,Moneda.Nombre,NoPartidas,SubTotal,Descto0Porc,IEPS,tOTAL,Estado  from FacturaCpa left join Moneda on FacturaCpa.CodigoMoneda = Moneda.CodigoMoneda ";

        if (ccReport.Estado == "Activo") {
           strSQL += "Where Estado = 'A' ";
           EstadoCtivo = true;
           strSQL = string.Format(strSQL, ccReport.Estado);
        }
        else {
           strSQL += "Where Estado = 'T' ";
           EstadoCtivo = true;
           strSQL = string.Format(strSQL, ccReport.Estado);
        }
        if (EstadoCtivo == true) {
           if (ccReport.MuestraTodosFactura == false) {
              if (ccReport.TodosProveedor == true) {
                 strSQL += "And NoFacturaCpa = '{0}' AND FechaFacturaCpa >= '{1}' AND FechaFacturaCpa <= '{2}' ";
                 strSQL = string.Format(strSQL, ccReport.CodigoFact, ccReport.FechaDel.ToString("ddMMyyyy").Trim(), ccReport.FechaAl.ToString("ddMMyyyy").Trim());
              }
              else {
                 strSQL += "And NoFacturaCpa = '{0}' AND FechaFacturaCpa >= '{1}' AND FechaFacturaCpa <= '{2}' And CodigoProveedor = '{3}'  ";
                 strSQL = string.Format(strSQL, ccReport.CodigoFact, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim(), ccReport.CodigoProveedor);
              }
           }
           else {
              if (ccReport.TodosProveedor == false) {
                 strSQL += "And FechaFacturaCpa >= '{0}' AND FechaFacturaCpa <= '{1}' And CodigoProveedor = '{2}'  ";
                 strSQL = string.Format(strSQL, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim(), ccReport.CodigoProveedor);
              }
              else {
                 strSQL += " AND FechaFacturaCpa >= '{0}' AND FechaFacturaCpa <= '{1}' ";
                 strSQL = string.Format(strSQL,  ccReport.FechaDel.ToString("yyyyMMdd").Trim(), ccReport.FechaAl.ToString("yyyyMMdd").Trim());
              }
           }
        }
        else {
           if (ccReport.MuestraTodosFactura == false) {
              if (ccReport.TodosProveedor == false) {
                 strSQL += "Where NoFacturaCpa = '{0}' AND FechaFacturaCpa >= '{1}' AND FechaFacturaCpa <= '{2}'  And CodigoProveedor = '{3}' ";
                 EstadoCtivo = false;
                 strSQL = string.Format(strSQL, ccReport.CodigoFact, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim(), ccReport.CodigoProveedor);
              }
              else {
                 strSQL += "Where NoFacturaCpa = '{0}' AND FechaFacturaCpa >= '{1}' AND FechaFacturaCpa <= '{2}'  ";
                 EstadoCtivo = false;
                 strSQL = string.Format(strSQL, ccReport.CodigoFact, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim());
              }
           }
           else {
              if (ccReport.TodosProveedor != false) {
                 strSQL += "Where FechaFacturaCpa >= '{0}' AND FechaFacturaCpa <= '{1}'  ";
                 EstadoCtivo = false;
                 strSQL = string.Format(strSQL, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim());
              }
              else {
                 strSQL += "Where FechaFacturaCpa >= '{0}' AND FechaFacturaCpa <= '{1}'  ";
                 EstadoCtivo = false;
                 strSQL = string.Format(strSQL, ccReport.FechaDel.ToString("dd/MM/yyyy").Trim(), ccReport.FechaAl.ToString("dd/MM/yyyy").Trim());
              }
           }
        }
       

        
        strSQL = string.Format(strSQL);
        DataSet caja = null;
        try {
           caja = new DataSet();
           if (cone.State != ConnectionState.Open) {
              cone.ConnectionString = cadAccesoADatos.cnString("STMonitor");
              cone.Open();
           }
           SqlCommand com = new SqlCommand();
           com.Connection = cone;
           com.CommandText = strSQL;
           SqlDataAdapter adapta = new SqlDataAdapter();
           adapta.SelectCommand = com;
           adapta.Fill(caja, "Provedor");
           cone.Close();
        }
        catch (Exception) { }
        return caja.Tables["Provedor"];
     }

     public List<Empresa> datos() {
        string[] arrParam = new string[3];
        string strSQL = "";
        strSQL = "Select Direccion,RFC From Empresa where CodigoEmpresa ='{0}'  and CodigoGrupo ='{0}'";
        strSQL = string.Format(strSQL, Comun.Sistema.sysCodigoEmpresa, Comun.Sistema.sysCodigoGrupo);
        SqlDataReader dr = SelectDR1(Comun.Sistema.sysDBBase, strSQL, arrParam, CommandBehavior.SingleResult);
        var resultado = new List<Empresa>();
        if (dr != null) {
           if (dr.HasRows) {
              while (dr.Read()) {
                 Empresa ccFac = new Empresa();
                 ccFac.DireccionEM = dr["Direccion"].ToString();
                 ccFac.RFCEM = dr["RFC"].ToString();
                 resultado.Add(ccFac);
              }
           }
        }
        return resultado;
     }

     public DataTable SelectDTTickett(string numPag) {
        string strSQL = "select *  from FacturaCpa   ";
        strSQL += "where NoFacturaCpa = '{0}'  ";
        strSQL = string.Format(strSQL, numPag);
        DataSet caja = null;
        try {
           caja = new DataSet();
           cone.Close();
           if (cone.State != ConnectionState.Open) {
              cone.ConnectionString = cadAccesoADatos.cnString("STMonitor");
              cone.Open();
           }
           SqlCommand com = new SqlCommand();
           com.Connection = cone;
           com.CommandText = strSQL;
           SqlDataAdapter adapta = new SqlDataAdapter();
           adapta.SelectCommand = com;
           adapta.Fill(caja, "Provedor");
           cone.Close();
        }
        catch (Exception) { }
        return caja.Tables["Provedor"];
     }

     public DataTable SelectDTPticketDet(string numPag) {
        string strSQL = "select LTrim(RTrim(FacturaCpaDet.CodigoProducto)) as CodigoProducto, Unidades" +
                " as unidades,PrecioUnitario,Descto1,IVA,LTrim(RTrim(DirProLot.Descripcion)) as Descripcion, (Unidades * PrecioUnitario) as Total,IVAPorc from FacturaCpaDet  inner join DirProLot on FacturaCpaDet.CodigoProducto = DirProLot.CodigoProducto  " +
                "where FacturaCpaDet.NoFacturaCpa = '{0}'  ";
        
        strSQL = string.Format(strSQL, numPag);
        DataSet caja = null;
        try {
           caja = new DataSet();
           cone.Close();
           if (cone.State != ConnectionState.Open) {
              cone.ConnectionString = cadAccesoADatos.cnString("STMonitor");
              cone.Open();
           }
           SqlCommand com = new SqlCommand();
           com.Connection = cone;
           com.CommandText = strSQL;
           SqlDataAdapter adapta = new SqlDataAdapter();
           adapta.SelectCommand = com;
           adapta.Fill(caja, "Provedor");
           cone.Close();
        }
        catch (Exception) { }
        return caja.Tables["Provedor"];
      }


   }
}
